# Le Bento de Shimako

*Le Bento de Shimako* est une petite application Shiny permettant de concevoir des repas pour chien équilibrés, en tenant compte d'une liste d'ingrédients disponibles et du profil du chien. L'application peut-être utilisée en local sur R, ou en ligne.

NB: à ce jour, l'application n'est pas encore déployée.

## Avertissement
Veuillez noter que l'auteur de l'application n'est pas vétérinaire, et que l'application n'a (à ce jour) pas être revue ou validée par une personne ayant ces compétences. À ce stade, aucune garantie ne peut être apportée quand à l'exactitude des informations ou des résultats donnés par l'application.

## License
Copyright 2023 Nicolas MINIER

L'application est distribuée sous licence Creative Commons ["CC BY-NC-SA 4.0"](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) (Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0)


# Guide d'utilisation

*Le Bento de Shimako* se présente sous la forme de quatres volets (en plus d'une page *Accueil*, où vous trouverez une partie des informations présentes ici-même) : *Mon chien*, *Mon garde-manger*, *Suggestions de recettes*, *Rapport*. Ces volets sont à consulter dans l'ordre pour progresser dans la composition d'un repas pour chien. Cette section vous guide dans votre progression.

## Mon chien

Ce premier volet a pour objectif de dresser le profil de votre chien, afin d'établir quels sont ses besoins nutritifs journaliers. Une première section (en haut) vous invite à renseigner sa race, son âge, son poids, ainsi que son niveau d'activité. Enfin, vous avez la possibilité de choisir un régime spécial pour tenir compte d'un statut particulier (stérilisé·e, enceinte, allaitante, sénior) ou d'un objectif que vous vous êtes fixé (perte de poids).

Dans une seconde section (en bas), vous trouverez à gauche le positionnement de votre chien parmi ceux de même race et de même sexe, de leurs 3 mois à l'âge adulte. Vous pouvez vous appuyer dessus pour décider d'un régime particulier pour votre chien, mais gardez en tête que si votre chien est plus grand que la moyenne, il est tout à fait normal qu'il soit également plus lourd que la moyenne. Ce graphique seul ne doit pas décider d'une mise au régime.

À droite dans la seconde section, vous trouverez le résumé des besoins nutritifs journaliers de votre chien, calculé en fonction des renseignements indiqués plus haut dans la première section. Pour plus de détail sur l'algorithme calculant ces besoins journaliers, vous pouvez vous reporter à la section #calcul-des-besoins-nutritifs-journaliers ci-après.

## Mon garde-manger

Ce deuxième volet de l'application établi la liste des ingrédients qui pourront être utilisés. Une première section concerne un complément alimentaire à considérer. Même si ce n'est pas obligatoire, un tel complément permet de s'assurer un filet de sécurité sur un certain nombre de nutriments essentiels et qui pourraient manquer dans les ingrédients de la recette selon leur qualité, méthode et temps de stockage, ou mode de cuisson. Une seconde section vous invite à lister tous les ingrédients à votre disposition, et que vous consentiriez à donner à votre chien. Notez que tous ne seront pas nécessairement utilisés. Une troisième section vous propose de choisir des ingrédients obligatoirement inclus dans la recette. Il est conseillé d'en choisir un ou deux, afin d'éviter à l'application d'avoir à explorer toutes les combinaisons possibles à partir des ingrédients de votre garde-manger. En revanche, il est relativement inutile d'imposer plus d'une viande/poisson, un féculent, un légume, et une matière grasse à la fois. Enfin, une dernière section vous demande d'indiquer la quantité de friandise que vous êtes succeptibles de donner à votre chien dans la journée concernée. En effet, il est important de soustraire leur apport (calorique notamment) afin que ces friandises et récompenses ne soient pas responsables d'une prise de poids.

## Suggestions de recettes

Ce volet établi une sélection de plusieurs recettes qui répondent aux mieux aux besoins nutritifs de votre chien. Afin d'ajuster les doses, il vous est demandé dans une première section d'indiquer la quantité que vous souhaitez cuisiner (pour la journée, ou pour un repas parmi 2 ou 3 repas quotidiens). Lorsque votre choix est fait, appuyez sur "Préparons un bento !" et attendez quelques instants que l'application explore différentes recettes possibles afin de sélectionner les meilleures combinaisons d'ingrédients et leur quantités.

La recette s'approchant au mieux des besoins nutritifs de votre chien sera sélectionnée par défaut, et s'affichera sous forme de graphique dans la partie inférieure du volet. Vous pouvez cependant choisir une autre recette parmi une sélection effectuée automatiquement par l'application, explorant des versions avec d'autres ingrédients, un nombre d'ingrédients différent. Si la recette préférée de l'application se trouve contenir des ingrédients dont il ne faut pas abuser (poissons gras, pomme de terre, *etc.*), des recettes alternatives ne contenant pas ces ingrédients vous seront également proposées, au cas où ces ingrédients figuraient déjà dans les précédents repas de votre chien et afin de vous aider à diversifier son alimentation.

## Rapport

Ce dernier volet est là pour produire une page qui soit facilement imprimable et réutilisable à l'avenir. Vous y trouverez le profil de votre chien, ainsi que la recette sélectionnée dans le volet *Suggestions de recettes* et sa composition nutritionnelle. Appuyez sur "Enregistrer au format .pdf" pour sauvegarder la fiche sur votre ordinateur.


# Calcul des besoins nutritifs journaliers

Les calculs de besoins nutritifs journaliers suivent des équations spécialement établies pour que les résultats donnés soient cohérents avec les recommandations de la *National Academy of Science* américaine rapportées dans [*"YOUR DOG’S NUTRITIONAL NEEDS. A Science-Based Guide For Pet Owners"*](https://nap.nationalacademies.org/resource/10668/dog_nutrition_final_fix.pdf).

## Besoins de référence

### Besoins caloriques

Les besoins caloriques journaliers (exprimés en kcal) pour un chien adulte sont donnés en fonction de la masse du chien (en kg), suivant l'équation généraliste suivante :

```math
Besoins = [-0,295*(masse+2,5)^2 + 61,2*(masse+2.5)]*Coefficient
```

Un coefficient pourra modifier ce besoin calorique calculé, selon plusieurs cas de figures :

Selon le niveau d'activité physique du chien, un coefficient vient modifier ces besoins, allant de x0,75 (pour un chien très sédentaire) et jusque x1,25 (pour un chien très sportif). Pour les chiennes enceintes, ce coefficient modificateur est de 1,4.

**Cas particulier : chiennes allaitantes**

Pour une chienne allaitante, les besoins caloriques journaliers sont donnés par l'équation suivante, qui inclut également le nombre de chiots allaités *N* :

```math
Besoins = [-1.97*(N+2)^2 + 31.0*(N+2)]*(masse+2.5)
```

Dans ce cas-ci, le niveau d'activité physique de la chienne n'est pas pris en compte.


### Besoins protéiques

chiot : 56g pour un chiot de 5.5kg devant grandir jusque 15kg à l'âge adulte
adulte : 25g pour 15kg
enceinte : 69g pour 15kg
allaitante : 158g pour 15kg allaitant six chiots

au moins 10% des calories prises par un adulte doit venir de protéines, 15% pour les plus vieux

### Besoins lipidiques

adulte : 14g pour 15kg
chiot : 21g pour un chiot de 5.5kg devant grandir jusque 15kg à l'âge adulte
enceinte : 29g pour 15kg
allaitante : 67g pour 15kg allaitant six chiots

au moins 5.5% des calories prises par un adulte doit venir de protéines

### Besoins glucidiques

Jusque 50% de l'apport calorique peut venir sous forme de glucides.
2.5-4.5% doivent venir sous forme de fibres


## Transition chiot-adulte

L'application *Le Bento de Shimako* opère une transition automatique et progressive entre un régime "chiot" et "adulte", tant en quantité qu'en composition. Un âge de transition est défini pour chaque race et sexe de chien, comme étant l'âge auquel 90% du trajet en poids est effectué entre le poids à 3 et le poids adulte stabilisé. Cet âge peut se trouver (typiquement) entre 8 mois pour les plus petits gabarits et 20 mois pour les races de chien les plus grandes.

Dans le cas des besoins caloriques, la transition s'opère dans dès les trois mois et jusque l'âge de transition correspondant au profil du chien. À trois mois, les besoins caloriques sont majorés de 66%, et cette majoration s'attenue de façon linéaire jusque l'âge de transition.

En ce qui concerne les besoins nutritionnels, une balance progressive s'opère entre une composition "chiot" maintenue jusque l'âge de transition. À partir de cet âge, une transition s'opère progressivement vers une composition "adulte", et s'achève quatre mois après l'âge de transition.